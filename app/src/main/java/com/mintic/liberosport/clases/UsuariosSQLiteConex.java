package com.mintic.liberosport.clases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class UsuariosSQLiteConex extends SQLiteOpenHelper {
    private static final String baseDatos = "dbg04";
    private static final int version = 1;

    public UsuariosSQLiteConex(@Nullable Context context)
    {
        super(context, baseDatos, null, version);
    }


    @Override
    public void onCreate (SQLiteDatabase db){
        db.execSQL("CREATE TABLE usuarios (\n" +
                "    id           INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "    nick         VARCHAR (50) NOT NULL,\n" +
                "    nombre       VARCHAR (60) NOT NULL,\n" +
                "    apellido     VARCHAR (60) NOT NULL,\n" +
                "    tipDocumento VARCHAR (20) NOT NULL,\n" +
                "    documento    INTEGER      NOT NULL,\n" +
                "    tipoDeUsuario VARCHAR (20) NOT NULL,\n" +
                "    email        VARCHAR (50) NOT NULL,\n" +
                "    password     VARCHAR (80) NOT NULL,\n" +
                "    direccion    VARCHAR (80) NOT NULL\n" +
                ");\n");

        db.execSQL("CREATE TABLE tienda (\n" +
        "  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "  nombre varchar(255),\n" +
                "  direccion varchar(255) default NULL,\n" +
                "  email varchar(255) default NULL,\n" +
                "  telefono varchar(100) default NULL,\n" +
                "  whatsapp varchar(100) default NULL,\n" +
                "  coordenada varchar(30) default NULL\n" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

package com.mintic.liberosport.clases;

import android.content.Context;
import android.content.Intent;

public class Navegacion {
    public static void IrA(Context contexto, Class<?> clase)
    {
        Intent intent = new Intent(contexto, clase);
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        contexto.startActivity(intent);
    }
}

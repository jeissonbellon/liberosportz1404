package com.mintic.liberosport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mintic.liberosport.dao.TiendaDao;
import com.mintic.liberosport.modelos.Coordenada;
import com.mintic.liberosport.modelos.Tienda;
import com.mintic.liberosport.viewmodel.UbicacionViewModel;

public class RegistroTiendaActivity extends AppCompatActivity {

    private EditText nombre;
    private EditText direccion;
    private EditText email;
    private EditText telefono;
    private EditText whatsapp;
    private EditText coordenada;
    private TextView titulo;
    private int codigoRegistro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_tienda);

        UbicacionViewModel ubicacionView = new ViewModelProvider(this).get(UbicacionViewModel.class);

        titulo = findViewById(R.id.agg_UserTienda);
        nombre = findViewById(R.id.agg_nombreTienda);
        direccion = findViewById(R.id.agg_dirTienda);
        email = findViewById(R.id.agg_telTienda);
        telefono = findViewById(R.id.agg_telTienda);
        whatsapp = findViewById(R.id.agg_waTienda);
        coordenada = findViewById(R.id.agg_coordTienda);

        Bundle datos = getIntent().getExtras();
        Button btnEliminar = findViewById(R.id.btn_eliminarTienda);

        if (datos != null) {
            codigoRegistro = datos.getInt("id");
            TiendaDao baseDatos = new TiendaDao(this);
            Tienda registro = baseDatos.listar(String.valueOf(codigoRegistro)).get(0);

            titulo.setText(registro.getNombre());
            nombre.setText(registro.getNombre());
            direccion.setText(registro.getDireccion());
            email.setText(registro.getEmail());
            telefono.setText(registro.getTelefono());
            whatsapp.setText(registro.getWhatsapp());
            coordenada.setText(registro.getCoordenada());

            Coordenada ubicacion = new Coordenada(
                    registro.getNombre(),
                    Double.parseDouble(registro.getCoordenada().split(",")[0]),
                    Double.parseDouble(registro.getCoordenada().split(",")[1]),
                    registro
            );

            UbicacionViewModel.latlon.setValue(ubicacion);

            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TiendaDao baseDatos = new TiendaDao(v.getContext());
                    baseDatos.eliminar(registro);
                    onBackPressed();
                    finish();
                }
            });
        } else {
            btnEliminar.setVisibility(View.GONE);
            ubicacionView.latlon.setValue(obtenerUbicacionActual());
        }
        Button btnVolver = findViewById(R.id.btn_volverTienda);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button btnGuardar = findViewById(R.id.agg_UserTienda);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SuspiciousIndentation")
            @Override
            public void onClick(View v) {
                Tienda objetoCargado = new Tienda(
                        codigoRegistro,
                        nombre.getText().toString(),
                        direccion.getText().toString(),
                        email.getText().toString(),
                        telefono.getText().toString(),
                        whatsapp.getText().toString(),
                        coordenada.getText().toString()
                );

                TiendaDao baseDatos = new TiendaDao(v.getContext());

                if (codigoRegistro > 0)
                    baseDatos.actualizar(objetoCargado);
                else
                    baseDatos.insertar(objetoCargado);
                onBackPressed();
                finish();
            }
        });
    }
    public Coordenada obtenerUbicacionActual(){
        Coordenada coordenadaActual = new Coordenada();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION }, 99);
            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION }, 99);
        }
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location localizacion = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        coordenadaActual.setLatitud(localizacion.getLatitude());
        coordenadaActual.setLongitud(localizacion.getLongitude());
        coordenadaActual.setNombre("Ubicación Actual");
        coordenadaActual.setInfoTienda(new Tienda());
        return coordenadaActual;
    }
}

package com.mintic.liberosport.modelos;

public class Coordenada {
    private Tienda infoTienda;
    private String nombre;
    private double latitud;
    private double longitud;

    public Coordenada() {
    }

    public Coordenada(String nombre, double latitud, double longitud, Tienda tienda){
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.infoTienda = tienda;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public Tienda getInfoTienda() {
        return infoTienda;
    }

    public void setInfoTienda(Tienda infoTienda) {
        this.infoTienda = infoTienda;
    }
}

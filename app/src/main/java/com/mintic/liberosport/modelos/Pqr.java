package com.mintic.liberosport.modelos;

import java.util.Date;

public class Pqr
{
    private String id;
    private String email;
    private String enunciado;
    private String mensaje;
    private Date fecha;

    public Pqr() {
    }

    public Pqr(String email, String enunciado, String mensaje, Date fecha) {
        this.email = email;
        this.enunciado = enunciado;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }

    public Pqr(String id, String email, String enunciado, String mensaje, Date fecha) {
        this.id = id;
        this.email = email;
        this.enunciado = enunciado;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }

        public String getId() {
        return id;
    }

        public void setId(String id) {
        this.id = id;
    }

        public String getEmail() {
        return email;
    }

        public void setEmail(String email) {
        this.email = email;
    }

        public String getEnunciado() {
        return enunciado;
    }

        public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

        public String getMensaje() {
        return mensaje;
    }

        public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

        public Date getFecha() {
        return fecha;
    }

        public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}

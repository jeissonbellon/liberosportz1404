package com.mintic.liberosport.modelos;

public class Tienda {

    private int id;
    private String nombre;
    private String direccion;
    private String email;
    private String telefono;
    private String whatsapp;
    private String coordenada;

    public Tienda() {
    }

    public Tienda(int id) {
        this.id = id;
    }

    public Tienda(String nombre, String direccion, String email, String telefono, String whatsapp, String coordenada) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
        this.whatsapp = whatsapp;
        this.coordenada = coordenada;
    }

    public Tienda(int id, String nombre, String direccion, String email, String telefono, String whatsapp, String coordenada) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
        this.whatsapp = whatsapp;
        this.coordenada = coordenada;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(String coordenada) {
        this.coordenada = coordenada;
    }

    @Override
    public String toString()
    {
        String info = "/***Tienda***/\n";
        info+= "Nombre: " + this.nombre + "\n";
        info+= "Dirección: " + this.direccion + "\n";
        return info;

    }
}

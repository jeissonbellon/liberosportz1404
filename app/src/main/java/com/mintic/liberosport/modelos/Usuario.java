package com.mintic.liberosport.modelos;

public class Usuario {
    private String id;
    private String nick;
    private String email;
    private String password;
    private String nombre;
    private String apellido;
    private String tipoDocumento;
    private String documento;
    private String direccionFoto;

    public Usuario(String s, String toString, String string, String s1, String toString1, String string1, String s2) {
    }

    public Usuario(String id, String nick, String email, String password, String nombre, String apellido, String tipoDocumento, String documento, String direccionFoto) {
        this.id = id;
        this.nick = nick;
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.direccionFoto = direccionFoto;
    }

    public Usuario(String nick, String email, String password, String nombre, String apellido, String tipoDocumento, String documento, String direccionFoto) {
        this.nick = nick;
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.direccionFoto = direccionFoto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getDireccionFoto() {
        return direccionFoto;
    }

    public void setDireccionFoto(String direccionFoto) {
        this.direccionFoto = direccionFoto;
    }
}



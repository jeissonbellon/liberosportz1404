package com.mintic.liberosport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.mintic.liberosport.adapters.UsuariosRecyclerViewAdapter;
import com.mintic.liberosport.dao.UsuarioDao;
import com.mintic.liberosport.modelos.Usuario;
import com.mintic.liberosport.Dialog.infoUsuarioDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;

public class ListadoUsuariosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        UsuarioDao baseDatos = new UsuarioDao(getString(R.string.bdUsuario));
        DatabaseReference myRef = baseDatos.getReference();

        myRef.addValueEventListener(new ValueEventListener() {
            @SuppressLint("NewApi")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayList<Usuario> registros = new ArrayList<Usuario>();
                dataSnapshot.getChildren().forEach(item -> {
                    HashMap<String, Object> datosObjeto = (HashMap<String, Object>) item.getValue();

                    Usuario registro = new Usuario(
                            item.getKey(),
                            datosObjeto.get("nick").toString(),
                            datosObjeto.get("email").toString(),
                            datosObjeto.get("password").toString(),
                            datosObjeto.get("nombre").toString(),
                            datosObjeto.get("apellido").toString(),
                            datosObjeto.get("tipoDocumento").toString(),
                            datosObjeto.get("documento").toString()
                    );

                    registros.add(registro);
                });
                RecyclerView recyclerUsuario = findViewById(R.id.listadoUsuarios_recycler);
                UsuariosRecyclerViewAdapter adaptadorUsuario = new UsuariosRecyclerViewAdapter(registros);
                recyclerUsuario.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                recyclerUsuario.setAdapter(adaptadorUsuario);
            }
            @Override
            public void onCancelled(DatabaseError error){

            }
        });
        FloatingActionButton btnAgregar = findViewById(R.id.listadoUsuarios_btnAgregar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoUsuarioDialogFragment.newInstance(null).show(getSupportFragmentManager(), null);
            }
        });
    }
    private void refrescarListado(){

    }
}
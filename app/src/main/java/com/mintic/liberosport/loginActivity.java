package com.mintic.liberosport;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.mintic.liberosport.dao.UsuarioDao;
import com.mintic.liberosport.modelos.Usuario;

public class loginActivity extends AppCompatActivity {
    private GoogleSignInOptions gso;
    private GoogleSignInClient gsc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_LiberoSport);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        gsc = GoogleSignIn.getClient(this, gso);

        GoogleSignInAccount cuentaActual = GoogleSignIn.getLastSignedInAccount(this);
        if(cuentaActual!=null)
            if(!cuentaActual.isExpired())
                IrAMenu();


        Button btnAcceder = findViewById(R.id.login_btnAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IrAMenu();
            }
        });

        ImageButton btnGoogle = findViewById(R.id.login_btiGoogle);
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(gsc.getSignInIntent(), 100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==100)
        {
            if(resultCode == RESULT_OK) {
                Task<GoogleSignInAccount> resultadoCuenta = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    resultadoCuenta.getResult(ApiException.class);
                    IrAMenu();
                } catch (ApiException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void IrAMenu()
    {
        finish();
        Intent intent = new Intent(this, MainUserMenuActivity.class);
        startActivity(intent);
    }
}
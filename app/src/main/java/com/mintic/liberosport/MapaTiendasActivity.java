package com.mintic.liberosport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.mintic.liberosport.dao.TiendaDao;
import com.mintic.liberosport.modelos.Coordenada;
import com.mintic.liberosport.modelos.Tienda;
import com.mintic.liberosport.viewmodel.UbicacionViewModel;

import java.util.ArrayList;

public class MapaTiendasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_tiendas);

        UbicacionViewModel modeloUbicaciones = new ViewModelProvider(this).get(UbicacionViewModel.class);

        TiendaDao basedatos = new TiendaDao(this);
        ArrayList<Tienda> registros = basedatos.listar(null);

        //  14.254564, -9.25148
        // [0] => 14.254564
        // [1] => -9.25148

        ArrayList<Coordenada> listadoCoordenadas = new ArrayList<Coordenada>();
        for(Tienda registro : registros)
            listadoCoordenadas.add(
                    new Coordenada(
                            registro.getNombre(),
                            Double.parseDouble(registro.getCoordenada().split(",")[0]),
                            Double.parseDouble(registro.getCoordenada().split(",")[1]),
                            registro)
            );

        modeloUbicaciones.listaCoordenadas.setValue(listadoCoordenadas);
    }
}

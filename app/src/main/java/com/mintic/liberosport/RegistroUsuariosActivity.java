package com.mintic.liberosport;

import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mintic.liberosport.dao.UsuarioDao;
import com.mintic.liberosport.modelos.Usuario;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;

public class RegistroUsuariosActivity extends AppCompatActivity {

    private EditText txtNick;
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText txtNombre;
    private EditText txtApellido;
    private EditText txtTipoDocumento;
    private EditText txtDocumento;
    private String codigoRegistro;
    private Uri direccionFoto;
    private ImageView imgFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        UsuarioDao baseDatos = new UsuarioDao("usuario");
        DatabaseReference myRef = baseDatos.getReference();

        txtNick = findViewById(R.id.agg_nickUsuario);
        txtEmail = findViewById(R.id.agg_emailUsuario);
        txtPassword = findViewById(R.id.agg_claveUsuario);
        txtNombre = findViewById(R.id.agg_nombreUsuario);
        txtApellido = findViewById(R.id.agg_apellUsuario);
        txtTipoDocumento = findViewById(R.id.aggUsu_TipDoc);
        txtDocumento = findViewById(R.id.agg_numeroDocUsuario);
        imgFoto = findViewById(R.id.formularioUsuario_imgFoto);

        Bundle datos = getIntent().getExtras();

        if (datos != null) {
            codigoRegistro = datos.getString("id");
            myRef.child(codigoRegistro).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    HashMap<String, Object> datosObjeto = (HashMap<String, Object>) snapshot.getValue();

                    Usuario registro = new Usuario(
                            snapshot.getKey(),
                            datosObjeto.get("nick").toString(),
                            datosObjeto.get("email").toString(),
                            datosObjeto.get("password").toString(),
                            datosObjeto.get("nombre").toString(),
                            datosObjeto.get("apellido").toString(),
                            datosObjeto.get("tipoDocumento").toString(),
                            datosObjeto.get("documento").toString()
                    );

                    if(datosObjeto.containsKey("direccionFoto")) {
                        imgFoto = findViewById(R.id.formularioUsuario_imgFoto);
                        registro.setDireccionFoto(datosObjeto.get("direccionFoto").toString());
                        direccionFoto = Uri.parse(registro.getDireccionFoto());
                    }
                    txtNick.setText(registro.getNick());
                    txtEmail.setText(registro.getEmail());
                    txtPassword.setText(registro.getPassword());
                    txtNombre.setText(registro.getNombre());
                    txtApellido.setText(registro.getApellido());
                    txtTipoDocumento.setText(registro.getTipoDocumento());
                    txtDocumento.setText(registro.getDocumento());
                    imgFoto.setImageURI(direccionFoto);
                }
                @Override
                public void onCancelled (@NonNull DatabaseError error){

                }
            });
        }
        Button btnGuardar = findViewById(R.id.agg_btnUsu);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario registro = new Usuario(
                        txtNick.getText().toString(),
                        txtEmail.getText().toString(),
                        txtPassword.getText().toString(),
                        txtNombre.getText().toString(),
                        txtApellido.getText().toString(),
                        txtTipoDocumento.getText().toString(),
                        txtDocumento.getText().toString()
                        );
                registro.setDireccionFoto(direccionFoto.toString());

                myRef.child(codigoRegistro).setValue(registro);
            }
        });

        Button btnEliminar = findViewById(R.id.agg_eliUsuario);
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myRef.child(codigoRegistro).removeValue();
            }
        });

        imgFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                    {
                        String[] permisos = { Manifest.permission.READ_EXTERNAL_STORAGE };
                        requestPermissions(permisos, 1001);
                    }
                    else
                        cargarImagen();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==1001)
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                cargarImagen();
            else
                Toast.makeText(this, "No tiene permisos.", Toast.LENGTH_LONG);
    }

    private void cargarImagen() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== RESULT_OK && requestCode == 100) {
            data.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            data.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            direccionFoto = data.getData();
            imgFoto.setImageURI(direccionFoto);
        }
    }

    private void solicitarpermisos()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
            {
                String[] permisos = { Manifest.permission.READ_EXTERNAL_STORAGE };
                requestPermissions(permisos, 1001);
            }
    }
}


package com.mintic.liberosport.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.mintic.liberosport.modelos.Usuario;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.UUID;

public class UsuarioDao {
    private String nombreReferencia;
    private FirebaseDatabase baseDatos;
    private DatabaseReference referencia;

    public UsuarioDao(String nombreReferencia) {
        this.nombreReferencia = nombreReferencia;
        this.baseDatos = FirebaseDatabase.getInstance();
        this.referencia = this.baseDatos.getReference(this.nombreReferencia);
        this.referencia.keepSynced(true);
    }

    public boolean guardar(Usuario usuario)
    {
        boolean guardo= false;
        try{

            referencia.child(UUID.randomUUID().toString()).setValue(usuario);
            guardo = true;
        }
        catch (Exception ex)
        {}
        return guardo;
    }
    public DatabaseReference getReference()
    {
        return this.referencia;
    }
}

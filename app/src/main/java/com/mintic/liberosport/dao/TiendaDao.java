package com.mintic.liberosport.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mintic.liberosport.clases.UsuariosSQLiteConex;
import com.mintic.liberosport.modelos.Tienda;

import java.util.ArrayList;

public class TiendaDao {
    private Context contexto;
    private final String nombreTabla = "tienda";
    private UsuariosSQLiteConex conexion;

    public TiendaDao(Context contexto) {
        this.contexto = contexto;
        this.conexion = new UsuariosSQLiteConex(this.contexto);
    }

    public long insertar(Tienda modeloTienda)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        ContentValues valoresAInsertar = new ContentValues();
        valoresAInsertar.put("nombre", modeloTienda.getNombre());
        valoresAInsertar.put("direccion", modeloTienda.getDireccion());
        valoresAInsertar.put("email", modeloTienda.getEmail());
        valoresAInsertar.put("telefono", modeloTienda.getTelefono());
        valoresAInsertar.put("whatsapp", modeloTienda.getWhatsapp());
        valoresAInsertar.put("coordenadas", modeloTienda.getCoordenada());

        return baseDatos.insert(this.nombreTabla, null, valoresAInsertar);
    }

    public ArrayList<Tienda> listar(String id) {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();
        baseDatos = this.conexion.getWritableDatabase();
        ArrayList<Tienda> registros = new ArrayList<Tienda>();

        String[] columnasAConsultar = {"id", "nombre", "direccion", "email", "telefono", "whatsapp", "coordenadas"};

        String condicionWhere = null;
        String[] valoresCondicionWhere = null;
        if (id != null && !id.isEmpty())
        {
            condicionWhere = "id = ?";
            valoresCondicionWhere = new String[]{id};
        }

        Cursor cursor = baseDatos.query(this.nombreTabla, columnasAConsultar, condicionWhere, valoresCondicionWhere, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();

            do {

                Tienda registro = new Tienda(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6)
                );

                registros.add(registro);

            } while (cursor.moveToNext());

            cursor.close();
        }

        return registros;
    }
    public int actualizar(Tienda modeloTienda)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        ContentValues valoresAActualizar = new ContentValues();
        valoresAActualizar.put("nombre", modeloTienda.getNombre());
        valoresAActualizar.put("direccion", modeloTienda.getDireccion());
        valoresAActualizar.put("email", modeloTienda.getEmail());
        valoresAActualizar.put("telefono", modeloTienda.getTelefono());
        valoresAActualizar.put("whatsapp", modeloTienda.getWhatsapp());
        valoresAActualizar.put("coordenada", modeloTienda.getCoordenada());

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = {String.valueOf(modeloTienda.getId()) };

        return baseDatos.update(this.nombreTabla, valoresAActualizar, criterioWhere, valoresCriterioWhere);
    }

    public int eliminar(Tienda modeloTienda)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = {String.valueOf(modeloTienda)};

        return baseDatos.delete(this.nombreTabla, criterioWhere, valoresCriterioWhere);
    }


}

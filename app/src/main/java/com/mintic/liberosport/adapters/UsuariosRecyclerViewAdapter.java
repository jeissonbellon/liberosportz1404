package com.mintic.liberosport.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mintic.liberosport.R;
import com.mintic.liberosport.RegistroUsuariosActivity;
import com.mintic.liberosport.modelos.Usuario;
import java.util.ArrayList;

public class UsuariosRecyclerViewAdapter extends RecyclerView.Adapter<UsuariosRecyclerViewAdapter.ViewHolderUsuarios> {

    private ArrayList<Usuario> registros;

    public UsuariosRecyclerViewAdapter(ArrayList<Usuario> registros) {
        this.registros = registros;}

    @NonNull
    @Override
    public ViewHolderUsuarios onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.listado_usuario_viewholder, null, false);
        return new ViewHolderUsuarios(vista);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderUsuarios holder, int position) {
        holder.cargarInformación(registros.get(position));
    }

    @Override
    public int getItemCount() {
        return registros.size() > 0 ? registros.size() : 1;
    }
    public class ViewHolderUsuarios extends RecyclerView.ViewHolder {

        String id;
        TextView nombreCompleto;
        TextView email;

        public ViewHolderUsuarios(@NonNull View itemView) {
            super(itemView);

            nombreCompleto = itemView.findViewById(R.id.listadoUsuarios_txvNombreCompleto);
            email = itemView.findViewById(R.id.listadoUsuarios_txvEmail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), RegistroUsuariosActivity.class);
                    intent.putExtra("id", id);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                    v.getContext().startActivity(intent);
                }
            });
        }
        public void cargarInformación(Usuario registro)
        {
            id = registro.getId();
            nombreCompleto.setText(registro.getNombre() + "" + registro.getApellido());
            email.setText(registro.getEmail());
        }
    }
}


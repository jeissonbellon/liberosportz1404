package com.mintic.liberosport.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mintic.liberosport.R;
import com.mintic.liberosport.RegistroTiendaActivity;
import com.mintic.liberosport.modelos.Tienda;

import java.util.ArrayList;

public class TiendaRecyclerViewAdapter extends RecyclerView.Adapter<TiendaRecyclerViewAdapter.ViewHolderTienda> {

    private ArrayList<Tienda> registros;

    public TiendaRecyclerViewAdapter(ArrayList<Tienda> registros){
        this.registros = registros;
    }
    @NonNull
    @Override
    public ViewHolderTienda onCreateViewHolder(@NonNull ViewGroup parent, int ViewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.listado_tienda_viewholder, null, false);
        return new ViewHolderTienda(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTienda holder, int position) {
        holder.cargarInformacion(registros.get(position));
    }


    @Override
    public int getItemCount() {
        return registros.size();
    }

    public class ViewHolderTienda extends RecyclerView.ViewHolder {

        int id;
        TextView nombre;
        TextView direccion;
        TextView telefono;
        TextView whatsapp;

        public ViewHolderTienda(@NonNull View itemView) {
            super(itemView);

            nombre = itemView.findViewById(R.id.nombre_tienda);
            direccion = itemView.findViewById(R.id.direccion_tienda);
            telefono = itemView.findViewById(R.id.telefono_tienda);
            whatsapp = itemView.findViewById(R.id.whatsapp_tienda);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), RegistroTiendaActivity.class);
                    intent.putExtra("id", id);
                    v.getContext().startActivity(intent);
                }
            });
        }
        public void cargarInformacion(Tienda registro)
        {
            nombre.setText(registro.getNombre());
            direccion.setText(registro.getDireccion());
            telefono.setText(registro.getTelefono());
            whatsapp.setText(registro.getWhatsapp());
        }
    }
}

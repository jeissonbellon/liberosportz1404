package com.mintic.liberosport.Dialog;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.mintic.liberosport.R;
import com.mintic.liberosport.dao.UsuarioDao;
import com.mintic.liberosport.modelos.Usuario;


public class infoUsuarioDialogFragment extends DialogFragment {

    private Usuario registro;
    private DialogFragment dialogo;

    public infoUsuarioDialogFragment(int contentLayoutId, Usuario Usuario){
        super(contentLayoutId);
        this.registro = Usuario;
        dialogo = this;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceID) {
        super.onViewCreated(view, savedInstanceID);

        TextView nick = view.findViewById(R.id.agg_nickUsuario);
        TextView email = view.findViewById(R.id.agg_emailUsuario);
        TextView password = view.findViewById(R.id.agg_claveUsuario);
        TextView nombre = view.findViewById(R.id.agg_nombreUsuario);
        TextView apellido = view.findViewById(R.id.agg_apellUsuario);
        TextView direccion = view.findViewById(R.id.agg_dirUsuario);
        TextView tipDoc = view.findViewById(R.id.aggUsu_TipDoc);
        TextView documento = view.findViewById(R.id.agg_numeroDocUsuario);
        TextView telefono = view.findViewById(R.id.agg_telefonoUsuario);
        Button guardar = view.findViewById(R.id.agg_btnUsu);
        guardar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Usuario registro = new Usuario(
                        nick.getText().toString(),
                        password.getText().toString(),
                        nombre.getText().toString(),
                        apellido.getText().toString(),
                        direccion.getText().toString(),
                        tipDoc.getText().toString(),
                        documento.getText().toString(),
                        telefono.getText().toString(),
                        email.getText().toString()
                );

                UsuarioDao usuario = new UsuarioDao("liberosport");
                usuario.guardar(registro);
                dialogo.dismiss();
            }
        });

    }
    public static infoUsuarioDialogFragment newInstance(Usuario usuario){
        Bundle args = new Bundle();

        infoUsuarioDialogFragment fragment = new infoUsuarioDialogFragment(R.layout.fragment_formulario_usuario, usuario);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.PantallaCompleta);
        this.getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}

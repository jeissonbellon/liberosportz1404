package com.mintic.liberosport.Dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.mintic.liberosport.R;
import com.mintic.liberosport.modelos.Tienda;

public class infoTiendaDialogFragment extends DialogFragment {

    private Tienda registro;

    public infoTiendaDialogFragment(int contentLayoutId, Tienda tienda){
        super(contentLayoutId);
        this.registro=tienda;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        TextView nombre =view.findViewById(R.id.agg_nombreTienda);
        TextView direccion =view.findViewById(R.id.agg_dirTienda);
        TextView telefono =view.findViewById(R.id.agg_telTienda);
        TextView email =view.findViewById(R.id.agg_emailTienda);

        nombre.setText(registro.getNombre());
        direccion.setText(registro.getDireccion());
        telefono.setText(registro.getTelefono());
        email.setText(registro.getEmail());
    }
    public static infoTiendaDialogFragment newInstance(Tienda registro){

        Bundle args = new Bundle();

        infoTiendaDialogFragment fragment = new infoTiendaDialogFragment(R.layout.activity_registro_tienda, registro);
        fragment.setArguments(args);
        return fragment;

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.PantallaCompleta);
    }
}

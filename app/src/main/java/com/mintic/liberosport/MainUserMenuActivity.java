package com.mintic.liberosport;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mintic.liberosport.clases.Navegacion;

public class MainUserMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_LiberoSport);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user_menu);

        Button btnTienda = findViewById(R.id.main_listarTiendas);
        btnTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ListadoTiendasActivity.class);
                startActivity(intent);
            }
        });
        Button btnUsuario = findViewById(R.id.main_adminUsuario);
        btnUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ListadoUsuariosActivity.class);
                startActivity(intent);
            }
        });
        Button btnMapa = findViewById(R.id.mapa_tiendas);
        btnMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navegacion.IrA(v.getContext(), MapaTiendasActivity.class);
            }
        });
        Button btnAcercaDe = findViewById(R.id.main_nosotros);
        btnAcercaDe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navegacion.IrA(v.getContext(), AcercaDeActivity.class);
            }
        });



        ImageButton btnSalir = findViewById(R.id.img_btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
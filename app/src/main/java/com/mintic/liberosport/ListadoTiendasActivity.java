package com.mintic.liberosport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mintic.liberosport.adapters.TiendaRecyclerViewAdapter;
import com.mintic.liberosport.dao.TiendaDao;
import com.mintic.liberosport.modelos.Tienda;

import java.util.ArrayList;

public class ListadoTiendasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_tiendas);

        FloatingActionButton btnTienda = findViewById(R.id.btn_aggTiendaNueva);
        btnTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), RegistroTiendaActivity.class);
                startActivity(intent);
            }
        });


    }
    @Override
    public void onResume(){
        super.onResume();
        refrescarListado();
    }
    private void refrescarListado()
    {
        RecyclerView recyclerTienda = findViewById(R.id.listadoTiendas_Tiendas);

        TiendaDao baseDatos = new TiendaDao(this);
        ArrayList<Tienda> resultadoObtenido = baseDatos.listar(null);

        TiendaRecyclerViewAdapter adaptadorTienda = new TiendaRecyclerViewAdapter(resultadoObtenido);
        recyclerTienda.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerTienda.setAdapter(adaptadorTienda);
    }
}
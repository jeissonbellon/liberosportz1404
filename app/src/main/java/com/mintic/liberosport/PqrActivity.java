package com.mintic.liberosport;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mintic.liberosport.modelos.Pqr;

import java.util.Date;

public class PqrActivity extends AppCompatActivity {

    private EditText email;
    private EditText enunciado;
    private EditText mensaje;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pqr);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        email = findViewById(R.id.pqr_txtEmail);
        enunciado = findViewById(R.id.pqr_txtEnunciado);
        mensaje = findViewById(R.id.pqr_txtMensaje);

        Button btnGuardar = findViewById(R.id.pqr_btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pqr registro = new Pqr(email.getText().toString(), enunciado.getText().toString(), mensaje.getText().toString(), new Date());

                db.collection("pqrs")
                        .add(registro)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                System.out.println("DocumentSnapshot added with ID: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                System.out.println("Error adding document" + e.getMessage());
                            }
                        });
            }
        });

        db.collection("pqrs")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                System.out.println(document.getId() + " => " + document.getData());
                            }
                        } else {
                            System.out.println("Error getting documents." + task.getException());
                        }
                    }
                });
    }
}


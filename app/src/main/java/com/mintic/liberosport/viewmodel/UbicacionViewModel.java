package com.mintic.liberosport.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mintic.liberosport.modelos.Coordenada;

import java.util.ArrayList;

public class UbicacionViewModel extends ViewModel {

    public static MutableLiveData<Coordenada> latlon = new MutableLiveData<>();
    public MutableLiveData<ArrayList<Coordenada>> listaCoordenadas = new MutableLiveData<>();

    public MutableLiveData<Coordenada> obtenerUbicacion(){
        if(latlon==null)
        {
            latlon = new MutableLiveData<>();
            latlon.setValue(new Coordenada());
        }
        return latlon;
    }

    public MutableLiveData<ArrayList<Coordenada>> getListaCoordenadas() {
        if(listaCoordenadas==null)
        {
            listaCoordenadas = new MutableLiveData<>();
            listaCoordenadas.setValue(new ArrayList<>());
        }
        return listaCoordenadas;
    }
}
